import {Component, Input} from '@angular/core';
import {Ingredient} from "../shared/ingredient.model";


@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent  {
@Input() ingredient! : Ingredient;

  itemArray : string[] = [];
  price = 20;


 ingredients : Ingredient[] = [
   new Ingredient('Meat', 0, 50, 'https://image.freepik.com/free-photo/raw-beef-steaks-with-spices-rosemary-flat-lay-top-view_2829-4886.jpg'),
   new Ingredient('Salad', 0, 5, 'https://thumbs.dreamstime.com/z/organic-vegetable-salad-green-frillice-iceberg-lettuce-isolated-white-background-151715371.jpg'),
   new Ingredient('Cheese', 0,20, 'https://image.shutterstock.com/z/stock-photo-large-selection-of-cheeses-on-typical-farmer-market-selective-focus-high-quality-photo-2042239193.jpg'),
   new Ingredient ('Bacon', 0, 30, 'https://www.mensjournal.com/wp-content/uploads/2019/05/bacon.jpg?w=900&h=506&crop=1&quality=86&strip=all'),
 ];


  pushIngredients(i: number) {
    this.itemArray.push(this.ingredients[i].name);
    this.ingredients[i].quantity++;
    const generalAmount = this.ingredients[i].getPrice();
    this.price+= generalAmount;
  }

  removeNeededIngreds(i:number) {
    if (this.itemArray.includes(this.ingredients[i].name)) {
      this.ingredients[i].quantity--;
      const item = this.itemArray.indexOf(this.ingredients[i].name);
      if (i > -1) {
        this.itemArray.splice(item, 1);
      }
    }

  }


}
