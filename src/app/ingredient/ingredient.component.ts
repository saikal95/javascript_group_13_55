import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient.model";


@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent {
  @Input() ingredient! : Ingredient;
  @Output() add = new EventEmitter();
  @Output() remove = new EventEmitter();


  pushNewIngreds(){
   this.add.emit();
  }

  removePushedIngreds(){
    this.remove.emit();
  }

}
