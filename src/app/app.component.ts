import { Component } from '@angular/core';
import {Ingredient} from "./shared/ingredient.model";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {



  burger: Ingredient[] = [];


  constructor() {
    this.createBurger();

  }

  createBurger(){
    this.burger.push(new Ingredient('meat', 0,20, 'jff'));
  }




}
